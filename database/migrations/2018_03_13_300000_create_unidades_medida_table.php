<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadesMedidaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidades_medida', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('sigla');
            $table->double('tasa_conversion');
            $table->enum('principal',['SI','NO'])->default('NO');
            $table->integer('tipo_unidad_medida_fk')->unsigned();
            $table->foreign('tipo_unidad_medida_fk')->references('id')->on('tipo_unidades_medida')->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidades_medida');
    }
}
