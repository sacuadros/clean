<?php $__env->startSection('htmlheader_title'); ?>
Graficas de mediciones
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentheader_title'); ?>
Graficas de mediciones
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentheader_description'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('main-content'); ?>
    <div class="container-fluid spark-screen">
        <?php echo $__env->make('admin.graficas.listado', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>               
        <?php echo $__env->make('admin.graficas.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>    
    </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('adminlte::page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>