<?php $__env->startSection('htmlheader_title'); ?>
Administración de tipos de unidades de medida	
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentheader_title'); ?>
   Tipos de unidades de medida
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentheader_description'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('main-content'); ?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-9 col-md-offset-1">

                <?php if(isset($records)): ?>
                    <?php echo $__env->make('admin.tipounidadesmedida.editar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php endif; ?>
                <?php if(empty($records)): ?>
                    <?php echo $__env->make('admin.tipounidadesmedida.crear', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php endif; ?>              
                <?php echo $__env->make('admin.tipounidadesmedida.listado', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('adminlte::page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>