<!-- REQUIRED JS SCRIPTS -->
<script src="<?php echo e(asset('js/graficas.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('plugins/highcharts/highcharts.js')); ?>" type="text/javascript"></script>
<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<script src="<?php echo e(url (mix('/js/app.js'))); ?>" type="text/javascript"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
