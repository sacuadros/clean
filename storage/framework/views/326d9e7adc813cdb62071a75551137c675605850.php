<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
       
    </div>
    <!-- Default to the left -->
    <strong> <?php echo e(config('app.name')); ?> Copyright &copy; <?php echo e(date("Y")); ?> </strong> 
</footer>
