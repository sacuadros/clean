<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Agregar Variable</h3>
        <div class="box-tools pull-right">
          <!-- Buttons, labels, and many other things can be placed here! -->
          <!-- Here is a label for example -->
         
        </div>
    <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <?php echo Form::open(['route'=>'variables.store', 'method'=>'POST', 'data-toggle'=>'validator', 'role'=>'form']); ?>

        <div class="box-body"> 
            <div class="form-group">
              <?php echo Form::label('nombre','Nombre',['class'=>'label label-default']); ?>

              <?php echo Form::text('nombre',null , ['class'=>'form-control','placeholder'=>'Ingrese el nombre','required']); ?>                               
            </div>
             <div class="form-group">
              <?php echo Form::label('tipos','Tipos de unidad',['class'=>'label label-default']); ?>

              <?php echo Form::select('tipos',$tipos,null,['class'=>'form-control','placeholder'=>'Seleccione un tipo de  unidad de medida']); ?>                        
            </div>
             <div class="form-group">           
              <?php echo Form::label('unidad_medida_fk','Unidad de medida',['class'=>'label label-default']); ?>

              <?php echo Form::select('unidad_medida_fk',['' => 'Seleccione una una opción'],null,['class'=>'form-control','required']); ?>                               
            </div>                                
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <?php echo Form::submit('Agregar' ,['class'=>'btn btn-primary']); ?> 
        </div>
        <!-- box-footer -->
    <?php echo Form::close(); ?>

</div>
<!-- /.box -->