<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Listado de registros creados</h3>
        <div class="box-tools pull-right">
          <!-- Buttons, labels, and many other things can be placed here! -->
          <!-- Here is a label for example -->
         
        </div>
    <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    
        <div class="box-body"> 
         	<table class="table table-hover">
				<thead>
					<th>Id</th>
					<th>Nombre</th>
					<th>Tipo de unidad</th>
                    <th>Sigla</th>
                    <th>Tasa de conversión</th>
                    <th>Principal</th>
                    <th>Acciones</th>
				</thead>
				<tbody>					
					<?php $__currentLoopData = $unidades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $unidad): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr>
							<td><?php echo e($unidad->id); ?></td>
							<td><?php echo e($unidad->nombre); ?></td>
                            <td><?php echo e($unidad->tipo_unidad_medida->nombre); ?></td>
                            <td><?php echo e($unidad->sigla); ?></td>
                            <td><?php echo e($unidad->tasa_conversion); ?></td>
                            <td><?php echo e($unidad->principal); ?></td>
							<td>
                                <a href="<?php echo e(route('unidadesmedida.edit', $unidad->id)); ?>" data-toggle="tooltip" title="Editar Registro" class="btn btn-warning "><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                <a href="<?php echo e(route('unidadesmedida.destroy', $unidad->id)); ?>" data-toggle="tooltip" title="Eliminar Registro" class="btn btn-danger "><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                            </td>
						</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</tbody>
			</table>			                    
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        	<?php echo e($unidades->links()); ?>    
        </div>
        <!-- box-footer -->    
</div>
<!-- /.box -->