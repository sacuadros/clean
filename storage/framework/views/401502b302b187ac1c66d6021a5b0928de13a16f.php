<!DOCTYPE html>
<!--
Landing page based on Pratt: http://blacktie.co/demo/pratt/
-->
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Adminlte-laravel - <?php echo e(trans('adminlte_lang::message.landingdescription')); ?> ">
    <meta name="author" content="Sergi Tur Badenas - acacha.org">

    <meta property="og:title" content="Adminlte-laravel" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="Adminlte-laravel - <?php echo e(trans('adminlte_lang::message.landingdescription')); ?>" />
    <meta property="og:url" content="http://demo.adminlte.acacha.org/" />
    <meta property="og:image" content="http://demo.adminlte.acacha.org/img/AcachaAdminLTE.png" />
    <meta property="og:image" content="http://demo.adminlte.acacha.org/img/AcachaAdminLTE600x600.png" />
    <meta property="og:image" content="http://demo.adminlte.acacha.org/img/AcachaAdminLTE600x314.png" />
    <meta property="og:sitename" content="demo.adminlte.acacha.org" />
    <meta property="og:url" content="http://demo.adminlte.acacha.org" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@acachawiki" />
    <meta name="twitter:creator" content="@acacha1" />

    <title><?php echo e(trans('adminlte_lang::message.landingdescriptionpratt')); ?></title>

    <!-- Custom styles for this template -->
    <link href="<?php echo e(asset('/css/all-landing.css')); ?>" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>

</head>

<body data-spy="scroll" data-target="#navigation" data-offset="50">

<div id="app" v-cloak>
    <!-- Fixed navbar -->
    <div id="navigation" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>               
                <a class="navbar-brand" href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset('img/morearHORIZONTAL13.png')); ?>" style="
                    width: 100px;
                    float: left;"
                 ></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#home" class="">Inicio</a></li>
                    <li><a href="#intro" class="">¿Porque Morear?</a></li>
                    <li><a href="#features" class="">Características</a></li>
                    <li><a href="#showcase" class="">¿Quienes somos?</a></li>
                    <li><a href="#contact" class="">Contacto</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php if(Auth::guest()): ?>
                        <li><a href="<?php echo e(url('/login')); ?>"><?php echo e(trans('adminlte_lang::message.login')); ?></a></li>
                        <li><a href="<?php echo e(url('/register')); ?>"><?php echo e(trans('adminlte_lang::message.register')); ?></a></li>
                    <?php else: ?>
                        <li><a href="/home"><?php echo e(Auth::user()->name); ?></a></li>
                    <?php endif; ?>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>


    <section id="home" name="home">
        <div id="headerwrap">     
            <div class="row">   
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active img-fluid"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
                    <div class="item active">
                     <img src="<?php echo e(asset('img/fondoinicio.jpg')); ?>" class="img-fluid" alt="New York">
                      <div class="carousel-caption">
                        <h3>Los Angeles</h3>
                        <p>LA is always so much fun!</p>
                      </div>
                    </div>

                    <div class="item">
                      <<img src="<?php echo e(asset('img/mora1.jpg')); ?>"  class="img-fluid" alt="New York">
                      <div class="carousel-caption">
                        <h3>Chicago</h3>
                        <p>Thank you, Chicago!</p>
                      </div>
                    </div>

                    <div class="item">
                      <img src="<?php echo e(asset('img/mora2.jpg')); ?>" alt="New York">
                      <div class="carousel-caption">
                        <h3>New York</h3>
                        <p>We love the Big Apple!</p>
                      </div>
                    </div>
                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                  </a>
            </div> 

        </div><!--/ #headerwrap -->
    </section>

    <section id="desc" name="desc">
        <!-- INTRO WRAP -->
        <div id="intro">
            <div class="container">
                <div class="row centered">
                    <h1>¿Porque Morear?</h1>
                    <br>
                    <br>
                    <div class="col-lg-4">
                        <img src="<?php echo e(asset('/img/graph.png')); ?>" alt="">                        
                        <p><strong>MOREAR</strong> es un software para la gestión de la  trazabilidad del cultivo de la mora. Esta pensado para servir de herramienta a productores agrícolas que vean en el  sistema de invernadero inteligente,  una opción para llevar a cabo sus cultivos.</p>
                    </div>
                    <div class="col-lg-4">
                        <img src="<?php echo e(asset('/img/responsive.png')); ?>" alt="">                      
                        <p><strong>MOREAR</strong> permite de forma ágil y fácil registrar, visualizar e informar, los diferentes datos, y actividades correspondientes al cultivo, permitiéndole al usuario conocer toda la información del proceso.</p>
                    </div>
                    <div class="col-lg-4">
                        <img src="<?php echo e(asset('/img/bell.png')); ?>" alt="">                        
                        <p><strong>MOREAR</strong> mantiene un sistema de informes, notificaciones y alertas de las actividades y parámetros dando seguridad a cada una de las etapas del cultivo.</p>
                    </div>
                </div>
                <br>
                <hr>
            </div> <!--/ .container -->
        </div><!--/ #introwrap -->

        <!-- FEATURES WRAP -->
        <div id="features">
            <div class="container">
                <div class="row">
                   
                    <br>
                    <br>
                    <div class="col-lg-6 centered">
                        <img class="centered" src="<?php echo e(asset('/img/mobile.png')); ?>" alt="">
                    </div>

                    <div class="col-lg-6">
                        <h3><?php echo e(trans('adminlte_lang::message.features')); ?></h3>
                        <br>
                        <!-- ACCORDION -->
                        <div class="accordion ac" id="accordion2">
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    </a>
                                </div><!-- /accordion-heading -->
                                <div id="collapseOne" class="accordion-body collapse in">
                                    <div class="accordion-inner">
                                        <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    </div><!-- /accordion-inner -->
                                </div><!-- /collapse -->
                            </div><!-- /accordion-group -->
                            <br>

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                                        Aenean ullamcorper purus lobortis viverra ullamcorper.
                                    </a>
                                </div>
                                <div id="collapseTwo" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    </div><!-- /accordion-inner -->
                                </div><!-- /collapse -->
                            </div><!-- /accordion-group -->
                            <br>

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                        Nam posuere orci sed erat porta tincidunt.
                                    </a>
                                </div>
                                <div id="collapseThree" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    </div><!-- /accordion-inner -->
                                </div><!-- /collapse -->
                            </div><!-- /accordion-group -->
                            <br>

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                                       Ut porttitor ligula sit amet lectus pulvinar congue.
                                    </a>
                                </div>
                                <div id="collapseFour" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    </div><!-- /accordion-inner -->
                                </div><!-- /collapse -->
                            </div><!-- /accordion-group -->
                            <br>
                        </div><!-- Accordion -->
                    </div>
                </div>
            </div><!--/ .container -->
        </div><!--/ #features -->
    </section>

    <section id="showcase" name="showcase">
        <div id="showcase">
            <div class="container">
                <div class="row">
                <h1 class="text-center">¿Quienes somos?</h1>
                <br>
                  <div class="c col-sm-6">
                    <div class="panel panel-primary">
                      <div class="panel-heading">MISIÓN</div>
                      <div class="panel-body">
                        <P class="text-justify">Promover la implementación de una herramienta tecnológica para los cultivadores de mora en el departamento de Santander, con el fin de optimizar el uso de los recursos y de ese modo ofrecer un producto de calidad; a su vez minimizar las fallas que un cultivador de manera tradicional no puede prever.</P><br><br><br><br><br>
                      </div>
                    </div>
                  </div>
                 <div class="col-sm-6">
                    <div class="panel panel-primary">
                      <div class="panel-heading">EQUIPO DE TRABAJO</div>
                      <div class="panel-body centered">
                        <div class="col-sm-6">
                            <div class="box-body box-profile centered">
                                <img style="height: 140px;" class="centered profile-user-img  img-circle" src="<?php echo e(asset('/img/IMG_20150903_234924.jpg')); ?>" alt="User profile picture">
                                <h3 class="profile-username text-center">Oscar Mauricio Díaz</h3>
                                <p class="text-muted text-center">Ingeniero Electronico</p>                          
                            </div>
                        </div>
                         <div class="col-sm-6">
                             <div class="box-body box-profile">
                                <img style="height: 140px;" class="centered profile-user-img  img-circle" src="<?php echo e(asset('/img/IMG-20180210-WA0002.jpg')); ?>" alt="User profile picture">
                                <h3 class="profile-username text-center">Walter Giovanny Cuadros Rincón</h3>
                                <p class="text-muted text-center">Ingeniero Electronico</p>                          
                            </div>
                        </div>
                           
                    </div>
                      </div>
                    </div>
                  
                  <div class=" col-sm-6">
                    <div class="panel panel-primary">
                      <div class="panel-heading">RESEÑA</div>
                      <div class="panel-body">
                       <p  class="text-justify">Las UTS   en su proceso de acercamiento a la comunidad santandereana, haciendo visible su misión institucional de formar profesionales con actitud creativa en los diversos campos de la ciencia, propuso a un grupo de estudiantes de último semestre de ingeniería electrónica la implementación de una herramienta web que acerque a los cultivadores de mora del departamento a la utilización de nuevas tecnologías que les permitan mejorar su productividad  obteniendo mejores ingresos económicos que dignifiquen su actividad teniendo en cuenta que nuestro departamento ocupa el segundo lugar a nivel nacional en área cultiva de mora .</p>
                      </div>
                    </div>
                  </div>
                  <div class=" col-sm-6">
                    <div class="panel panel-primary">
                      <div class="panel-heading">VISIÓN</div>
                      <div class="panel-body">
                       <p class="text-justify">Ofrecer a la comunidad de cultivadores de mora en Santander la implementación de este sistema que hará un aporte al desarrollo tecnológico de la agricultura en la región; a su vez propiciará mejores caminos para alcanzar la rentabilidad y crecimiento económico de los procesos de producción al optimizar las técnicas usadas para el cultivo de la mora.</p><br><br><br><br><br>
                      </div>
                    </div>
                  </div>
                </div>
                <br>
                <br>
                <br>
            </div><!-- /container -->
        </div>
    </section>    

    <section id="contact" name="contact">
        <div id="footerwrap">
            <div class="container">
                <div class="col-lg-5">
                    <h3><?php echo e(trans('adminlte_lang::message.address')); ?></h3>
                    <p>
                        Av. Greenville 987,<br/>
                        New York,<br/>
                        90873<br/>
                        United States
                    </p>
                </div>

                <div class="col-lg-7">
                    <h3><?php echo e(trans('adminlte_lang::message.dropus')); ?></h3>
                    <br>
                    <form role="form" action="#" method="post" enctype="plain">
                        <div class="form-group">
                            <label for="name1"><?php echo e(trans('adminlte_lang::message.yourname')); ?></label>
                            <input type="name" name="Name" class="form-control" id="name1" placeholder="<?php echo e(trans('adminlte_lang::message.yourname')); ?>">
                        </div>
                        <div class="form-group">
                            <label for="email1"><?php echo e(trans('adminlte_lang::message.emailaddress')); ?></label>
                            <input type="email" name="Mail" class="form-control" id="email1" placeholder="<?php echo e(trans('adminlte_lang::message.enteremail')); ?>">
                        </div>
                        <div class="form-group">
                            <label><?php echo e(trans('adminlte_lang::message.yourtext')); ?></label>
                            <textarea class="form-control" name="Message" rows="3"></textarea>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-large btn-success"><?php echo e(trans('adminlte_lang::message.submit')); ?></button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div id="c">
            <div class="container">
                <p>
                    
                    <strong><?php echo e(config('app.name')); ?> Copyright &copy; 2018</strong> 
                </p>

            </div>
        </div>
    </footer>

</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo e(url (mix('/js/app-landing.js'))); ?>"></script>
<script>
    $('.carousel').carousel({
        interval: 3500
    })
</script>
</body>
</html>
