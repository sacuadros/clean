<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Listado de registros creados</h3>
        <div class="box-tools pull-right">
          <!-- Buttons, labels, and many other things can be placed here! -->
          <!-- Here is a label for example -->
         
        </div>
    <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    
        <div class="box-body"> 
         	<table class="table table-hover">
				<thead>
					<th>Id</th>
					<th>Nombre</th>
					<th>Unidad de medida</th>                    
                    <th>Acciones</th>
				</thead>
				<tbody>					
					<?php $__currentLoopData = $variables; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $variable): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr>
							<td><?php echo e($variable->id); ?></td>
							<td><?php echo e($variable->nombre); ?></td>
                            <td><?php echo e($variable->unidad_medida->nombre); ?></td>                           
							<td>
                                <a href="<?php echo e(route('variables.edit', $variable->id)); ?>" data-toggle="tooltip" title="Editar Registro" class="btn btn-warning "><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                <a href="<?php echo e(route('variables.destroy', $variable->id)); ?>" data-toggle="tooltip" title="Eliminar Registro" class="btn btn-danger "><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                            </td>
						</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</tbody>
			</table>			                    
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        	<?php echo e($variables->links()); ?>    
        </div>
        <!-- box-footer -->    
</div>
<!-- /.box -->