<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Editar unidad de medida: <?php echo e($records->nombre); ?></h3>
        <div class="box-tools pull-right">
          <!-- Buttons, labels, and many other things can be placed here! -->
          <!-- Here is a label for example -->
         
        </div>
    <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <?php echo Form::open(['route'=> ['unidadesmedida.update',$records], 'method'=>'PUT']); ?>

        <div class="box-body"> 
            <div class="form-group">
              <?php echo Form::label('nombre','Nombre',['class'=>'label label-default']); ?>

              <?php echo Form::text('nombre',$records->nombre , ['class'=>'form-control','placeholder'=>'Ingrese el nombre','required']); ?>                               
            </div>
             <div class="form-group">           
              <?php echo Form::label('tipo_unidad_medida_fk','Tipo de unidad',['class'=>'label label-default']); ?>

              <?php echo Form::select('tipo_unidad_medida_fk',$tipos,$records->tipo_unidad_medida->id,['class'=>'form-control','placeholder'=>'Seleccione un tipo','required']); ?>                               
            </div>
             <div class="form-group">
              <?php echo Form::label('sigla','Sigla',['class'=>'label label-default']); ?>

              <?php echo Form::text('sigla',$records->sigla , ['class'=>'form-control','placeholder'=>'Ingrese la sigla o abreviación','required','maxlength'=>'10']); ?>                               
            </div>
            <div class="form-group">
              <?php echo Form::label('tasa_conversion','Tasa de conversion',['class'=>'label label-default']); ?>

              <?php echo Form::number('tasa_conversion',$records->tasa_conversion , ['class'=>'form-control','placeholder'=>'Ingrese la tasa de conversion','required','max'=>'99999','min'=>'0', 'data-bind'=>'value:replyNumber']); ?>                               
            </div>            
            <div class="checkbox">
                <label>
                    <?php echo Form::checkbox('principal', 'SI',$records->principal,['class'=>'form-check-input']); ?>

                    Principal 
                </label>               
            </div>           
                                 
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <?php echo Form::submit('Editar' ,['class'=>'btn btn-primary']); ?> 
        </div>
        <!-- box-footer -->
    <?php echo Form::close(); ?>

</div>
<!-- /.box -->