<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
	protected $table = "variables";

	protected $fillable = ['nombre','unidad_medida_fk'];

	public function mediciones(){

		return $this->hasMany("App\Medicion");		
	}
	public function unidad_medida(){

		return $this->belongsTo('App\UnidadMedida', 'unidad_medida_fk');  		
	}
}
