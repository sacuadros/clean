<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Medicion;
use App\Variable;


/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        
        $variables = Variable::orderBy('nombre','Desc')->pluck('nombre', 'id');         
        return view('adminlte::home')->with('variables',$variables);
    }

    public function livegrafica($id)
    {   
        $fecha_inicial = date('Y-m-d'.' H:i:s', time()-10); //need a space after dates.
        $fecha_final = date('Y-m-d'.' H:i:s', time());         
        $medicion =Medicion::where('variable_fk','=', $id)->where('created_at','>=',$fecha_inicial)->orderBy('id', 'desc')->first();
        if($medicion!=null){
        $retorno=$medicion->valor;
        } 
            else{
                 $retorno=0;       
        }       
        //Set the JSON header
        header("Content-type: text/json");
        // The x value is the current JavaScript time, which is the Unix time multiplied 
        // by 1000.

        $x = (time()-18000) * 1000;       
        // The y value is a random number                
        $y = $retorno;

        // Create a PHP array and echo it as JSON
        $ret = array($x, $y);        
        return json_encode($ret);

    }
}
