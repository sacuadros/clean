<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoUnidadMedida;
use Laracasts\Flash\FlashServiceProvider;

class TipoUnidadesMedidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipounidades = TipoUnidadMedida::orderBy('id','DESC')->paginate(5);
        return view('admin.tipounidadesmedida.tipounidadesmedida')->with('tipounidades', $tipounidades);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tipounidadesmedida.tipounidadesmedida');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tipounidad= new TipoUnidadMedida($request->all());
        $request->validate([
        'nombre' => 'required|unique:tipo_unidades_medida,nombre|max:255|alpha_spaces',        
        ]);
        $tipounidad->save();
        flash('Se ha insertado '.$tipounidad->nombre.' de forma exitosa!')->success()->important();
        return redirect()->route('tipounidadesmedida.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $records = TipoUnidadMedida::find($id);        
        $tipounidades = TipoUnidadMedida::orderBy('id','DESC')->paginate(5);
        return view('admin.tipounidadesmedida.tipounidadesmedida')->with(['records'=> $records,'tipounidades'=> $tipounidades]);      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $tipounidad = TipoUnidadMedida::find($id);
      $tipounidad->fill($request->all());     
      $tipounidad->save();
      flash('Se ha editado '.$tipounidad->nombre.' de forma exitosa!')->success()->important();
      return redirect()->route('tipounidadesmedida.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $tipounidad = TipoUnidadMedida::find($id);      
       $tipounidad->delete();
       flash('Se ha eliminado '.$tipounidad->nombre.' de forma exitosa!')->warning()->important();
       return redirect()->route('tipounidadesmedida.index');
    }
}
