<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoUnidadMedida;
use App\UnidadMedida;
use App\Variable;
use Illuminate\Support\Facades\DB;
class VariablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()    
    {
        $variables= Variable::orderBy('nombre','DESC')->paginate(5);
        $tipos = TipoUnidadMedida::orderBy('nombre','Desc')->pluck('nombre', 'id');                
        return view('admin.variables.variables')->with(['tipos'=> $tipos,'variables'=>$variables]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.variables.variables');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $variable= new Variable($request->all());
        $variable->unidad_medida_fk = $request->unidad_medida_fk;      
        $variable->save();
        flash('Se ha insertado '.$variable->nombre.' de forma exitosa!')->success()->important();
        return redirect()->route('variables.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $records = Variable::find($id);
        $records->each(function($records){
            $records->unidad_medida;
        });
         
        $unidades = UnidadMedida::find( $records->unidad_medida->id);
        $unidades->each(function($unidades){
            $unidades->tipo_unidad_medida;
        });
        $unidadesr = DB::table('unidades_medida')->where('tipo_unidad_medida_fk',$unidades->tipo_unidad_medida_fk )->get();
        $retorno=$unidadesr->pluck('nombre', 'id');                            
        $variables= Variable::orderBy('nombre','DESC')->paginate(5);      
        $tipos = TipoUnidadMedida::orderBy('nombre','DESC')->pluck('nombre', 'id');       
        return view('admin.variables.variables')->with(['records'=> $records,'tipos'=> $tipos, 'variables'=>$variables,'unidades'=>$unidades,'retorno'=>$retorno]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $variable = Variable::find($id);        
        $variable->fill($request->all());     
        $variable->save();
        flash('Se ha editado '.$variable->nombre.' de forma exitosa!')->success()->important();
        return redirect()->route('variables.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $variable = Variable::find($id);      
        $variable->delete();
        flash('Se ha eliminado '.$variable->nombre.' de forma exitosa!')->warning()->important();
        return redirect()->route('variables.index');
    }
}
