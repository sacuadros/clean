<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoUnidadMedida;
use App\UnidadMedida;
use Laracasts\Flash\FlashServiceProvider;

class UnidadesMedidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos = TipoUnidadMedida::orderBy('nombre','Desc')->pluck('nombre', 'id');
        $unidades=UnidadMedida::orderBy('id','Desc')->paginate(5);
        $unidades->each(function($unidades){
        $unidades->tipo_unidad_medida;
        });             
        return view('admin.unidadesmedida.unidadesmedida')->with(['tipos'=> $tipos,'unidades'=> $unidades]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.unidadesmedida.unidadesmedida');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $unidad= new UnidadMedida($request->all());
        $unidad->tipo_unidad_medida_fk = $request->tipo_unidad_medida_fk;      
        $unidad->save();
        flash('Se ha insertado '.$unidad->nombre.' de forma exitosa!')->success()->important();
        return redirect()->route('unidadesmedida.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $records = UnidadMedida::find($id);
        $records->each(function($records){
            $records->tipo_unidad_medida;
        });
        if($records->principal=='SI')
        {
            $records->principal=true;
        }
        else{
             $records->principal=false;
        }       
        $unidades = UnidadMedida::orderBy('id','DESC')->paginate(5);
        $unidades->each(function($unidades){
            $unidades->tipo_unidad_medida;
        });  
        $tipos = TipoUnidadMedida::orderBy('nombre','DESC')->pluck('nombre', 'id');       
        return view('admin.unidadesmedida.unidadesmedida')->with(['records'=> $records,'unidades'=> $unidades,'tipos'=> $tipos]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
      $unidad = UnidadMedida::find($id);
      if ($request->principal==null){$request->principal='NO';}
      $unidad->principal=$request->principal;
      $unidad->fill($request->all());     
      $unidad->save();
      flash('Se ha editado '.$unidad->nombre.' de forma exitosa!')->success()->important();
      return redirect()->route('unidadesmedida.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unidad = UnidadMedida::find($id);      
        $unidad->delete();
        flash('Se ha eliminado '.$unidad->nombre.' de forma exitosa!')->warning()->important();
        return redirect()->route('unidadesmedida.index');
    }
}
