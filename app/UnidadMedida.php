<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnidadMedida extends Model
{
    protected $table = "unidades_medida";

	protected $fillable = ['nombre','sigla','tasa_conversion','principal','tipo_unidad_medida_fk'];

	public function tipo_unidad_medida()
    {
        return $this->belongsTo('App\TipoUnidadMedida', 'tipo_unidad_medida_fk');      
    }
    public function variables()
    {
        return $this->hasMany("App\Variables");     
    }
}
