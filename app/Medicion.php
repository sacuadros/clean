<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicion extends Model
{
    protected $table = "mediciones";

	protected $fillable = ['valor','variable_fk'];

	public function variable (){

		return $this->belongsTo("App\Variable");
	}
}
