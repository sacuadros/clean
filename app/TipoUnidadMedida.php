<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoUnidadMedida extends Model
{
    protected $table = "tipo_unidades_medida";

	protected $fillable = ['nombre'];	

	public function unidades_medida()
    {
        return $this->hasMany('App\UnidadMedida');
    }
}
