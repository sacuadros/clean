<script >
		window.onload = function() {
		cargar_grafica_live();	
		};
		var chart; // global
        var t;
/**
 * Request data from the server, add it to the graph and set a timeout to request again
 */
function requestData() {
    var variable=$("#variable").val();
    var nombre=$("#variable").find('option:selected').text();       
    $.ajax({                
        url: 'live_grafica/'+variable, 
        success: function(point) {
            var series = chart.series[0],
                shift = series.data.length > 20; // shift if the series is longer than 20
                chart.series[0].name=nombre;                        
            // add the point
            chart.series[0].addPoint(eval(point), true, shift);

            
            // call it again after one second          
            t=setTimeout(requestData, 5000);  
        },
        cache: false
    });
}
function cargar_grafica_live(){
    var nombre=$("#variable").find('option:selected').text();
    clearTimeout(t);
    chart = new Highcharts.Chart({
        chart: {
            renderTo: 'live_container',
            defaultSeriesType: 'spline',
            events: {
                load: requestData
            }
        },
        title: {
            text: 'Grafico en tiempo real de mediciones'
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150,
            maxZoom: 20 * 1000
        },
        yAxis: {
            minPadding: 0.2,
            maxPadding: 0.2,
            title: {
                text: 'Valor',
                margin: 80
            }
        },
        series: [{
            name: nombre,
            data: []
        }]
    });     

}
</script>