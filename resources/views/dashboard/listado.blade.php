<!-- Default box -->
<div class="box box-primary">
		<div class="box-header">
			<div class="col-md-4">                 
	                  {{Form::label('variable','Variable',['class'=>'label label-default'])}}
	                  {!! Form::select('variable',$variables,null,['class'=>'form-control','id'=> 'variable','onchange'=>'cargar_grafica_live();']) !!}                
 
			</div>			
		</div>

		<div class="box-body" id="live_container">
		</div>

	    <div class="box-footer">
		</div>
	</div>
<!-- /.box -->