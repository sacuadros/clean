@extends('adminlte::page')

@section('htmlheader_title')
Administración de unidades de medida
@endsection
@section('contentheader_title')
   Unidades de medida
@endsection
@section('contentheader_description')
    
@endsection
@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-9 col-md-offset-1">

                @isset($records)
                    @include('admin.unidadesmedida.editar')
                @endisset
                @empty($records)
                    @include('admin.unidadesmedida.crear')
                @endempty              
                @include('admin.unidadesmedida.listado')
			</div>
		</div>
	</div>
@endsection
