<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Agregar unidad de medida</h3>
        <div class="box-tools pull-right">
          <!-- Buttons, labels, and many other things can be placed here! -->
          <!-- Here is a label for example -->
         
        </div>
    <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    {!! Form::open(['route'=>'unidadesmedida.store', 'method'=>'POST', 'data-toggle'=>'validator', 'role'=>'form']) !!}
        <div class="box-body"> 
            <div class="form-group">
              {!! Form::label('nombre','Nombre',['class'=>'label label-default']) !!}
              {!! Form::text('nombre',null , ['class'=>'form-control','placeholder'=>'Ingrese el nombre','required']) !!}                               
            </div>
             <div class="form-group">           
              {!! Form::label('tipo_unidad_medida_fk','Tipo de unidad',['class'=>'label label-default']) !!}
              {!! Form::select('tipo_unidad_medida_fk',$tipos,null,['class'=>'form-control','placeholder'=>'Seleccione un tipo','required']) !!}                               
            </div>
             <div class="form-group">
              {!! Form::label('sigla','Sigla',['class'=>'label label-default']) !!}
              {!! Form::text('sigla',null , ['class'=>'form-control','placeholder'=>'Ingrese la sigla o abreviación','required','maxlength'=>'10']) !!}                               
            </div>
            <div class="form-group">
              {!! Form::label('tasa_conversion','Tasa de conversion',['class'=>'label label-default']) !!}
              {!! Form::number('tasa_conversion',null , ['class'=>'form-control','placeholder'=>'Ingrese la tasa de conversion','required','max'=>'99999','min'=>'0', 'data-bind'=>'value:replyNumber']) !!}                               
            </div>            
            <div class="checkbox">
                <label>
                    {!! Form::checkbox('principal', 'SI',false,['class'=>'form-check-input'])!!}
                    Principal 
                </label>               
            </div>           
                                 
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {!! Form::submit('Agregar' ,['class'=>'btn btn-primary']) !!} 
        </div>
        <!-- box-footer -->
    {!! Form::close() !!}
</div>
<!-- /.box -->