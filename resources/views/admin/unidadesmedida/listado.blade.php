<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Listado de registros creados</h3>
        <div class="box-tools pull-right">
          <!-- Buttons, labels, and many other things can be placed here! -->
          <!-- Here is a label for example -->
         
        </div>
    <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    
        <div class="box-body"> 
         	<table class="table table-hover">
				<thead>
					<th>Id</th>
					<th>Nombre</th>
					<th>Tipo de unidad</th>
                    <th>Sigla</th>
                    <th>Tasa de conversión</th>
                    <th>Principal</th>
                    <th>Acciones</th>
				</thead>
				<tbody>					
					@foreach($unidades as $unidad)
						<tr>
							<td>{{ $unidad->id}}</td>
							<td>{{ $unidad->nombre}}</td>
                            <td>{{ $unidad->tipo_unidad_medida->nombre}}</td>
                            <td>{{ $unidad->sigla}}</td>
                            <td>{{ $unidad->tasa_conversion}}</td>
                            <td>{{ $unidad->principal}}</td>
							<td>
                                <a href="{{ route('unidadesmedida.edit', $unidad->id) }}" data-toggle="tooltip" title="Editar Registro" class="btn btn-warning "><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                <a href="{{ route('unidadesmedida.destroy', $unidad->id) }}" data-toggle="tooltip" title="Eliminar Registro" class="btn btn-danger "><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                            </td>
						</tr>
					@endforeach
				</tbody>
			</table>			                    
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        	{{ $unidades->links() }}    
        </div>
        <!-- box-footer -->    
</div>
<!-- /.box -->