@extends('adminlte::page')

@section('htmlheader_title')
	Administración de variables
@endsection
@section('contentheader_title')
   Variables
@endsection
@section('contentheader_description')
    
@endsection
@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-9 col-md-offset-1">

                @isset($records)
                    @include('admin.variables.editar')
                @endisset
                @empty($records)
                    @include('admin.variables.crear')
                @endempty              
                @include('admin.variables.listado')
                @include('admin.variables.scripts')
			</div>
		</div>
	</div>

@endsection

