<script type="text/javascript" language="javascript" class="init">
	window.onload = function() {			
		$('#tipos').change(function(){
			$.get("{{ url('dropdown')}}",
			{ option: $(this).val() },
			function(data) {
				$('#unidad_medida_fk').empty();
				$.each(data, function(key, element) {
					$('#unidad_medida_fk').append("<option value='" + key + "'>" + element + "</option>");
				});
			});
		});
	};
		
</script>
