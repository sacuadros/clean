<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Agregar Variable</h3>
        <div class="box-tools pull-right">
          <!-- Buttons, labels, and many other things can be placed here! -->
          <!-- Here is a label for example -->
         
        </div>
    <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    {!! Form::open(['route'=>'variables.store', 'method'=>'POST', 'data-toggle'=>'validator', 'role'=>'form']) !!}
        <div class="box-body"> 
            <div class="form-group">
              {!! Form::label('nombre','Nombre',['class'=>'label label-default']) !!}
              {!! Form::text('nombre',null , ['class'=>'form-control','placeholder'=>'Ingrese el nombre','required']) !!}                               
            </div>
             <div class="form-group">
              {!! Form::label('tipos','Tipos de unidad',['class'=>'label label-default']) !!}
              {!! Form::select('tipos',$tipos,null,['class'=>'form-control','placeholder'=>'Seleccione un tipo de  unidad de medida']) !!}                        
            </div>
             <div class="form-group">           
              {!! Form::label('unidad_medida_fk','Unidad de medida',['class'=>'label label-default']) !!}
              {!! Form::select('unidad_medida_fk',['' => 'Seleccione una una opción'],null,['class'=>'form-control','required']) !!}                               
            </div>                                
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {!! Form::submit('Agregar' ,['class'=>'btn btn-primary']) !!} 
        </div>
        <!-- box-footer -->
    {!! Form::close() !!}
</div>
<!-- /.box -->