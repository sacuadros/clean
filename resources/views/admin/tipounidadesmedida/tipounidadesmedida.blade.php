@extends('adminlte::page')

@section('htmlheader_title')
Administración de tipos de unidades de medida	
@endsection
@section('contentheader_title')
   Tipos de unidades de medida
@endsection
@section('contentheader_description')
    
@endsection
@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-9 col-md-offset-1">

                @isset($records)
                    @include('admin.tipounidadesmedida.editar')
                @endisset
                @empty($records)
                    @include('admin.tipounidadesmedida.crear')
                @endempty              
                @include('admin.tipounidadesmedida.listado')
			</div>
		</div>
	</div>
@endsection
