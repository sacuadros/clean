<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Agregar tipo de unidad de medida</h3>
        <div class="box-tools pull-right">
          <!-- Buttons, labels, and many other things can be placed here! -->
          <!-- Here is a label for example -->
         
        </div>
    <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    {!! Form::open(['route'=>'tipounidadesmedida.store', 'method'=>'POST', 'data-toggle'=>'validator', 'role'=>'form']) !!}
        <div class="box-body"> 
            <div class="form-group">
              {!! Form::label('nombre','Nombre',['class'=>'label label-default']) !!}
              {!! Form::text('nombre',null , ['class'=>'form-control','placeholder'=>'Ingrese el nombre','required']) !!}                               
            </div>                       
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {!! Form::submit('Agregar' ,['class'=>'btn btn-primary']) !!} 
        </div>
        <!-- box-footer -->
    {!! Form::close() !!}
</div>
<!-- /.box -->