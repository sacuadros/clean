@extends('adminlte::page')

@section('htmlheader_title')
Administración de tokens
@endsection
@section('contentheader_title')
Administración de tokens
@endsection
@section('contentheader_description')
    
@endsection
@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">                                           
                @include('admin.tokens.listado')
            </div>
        </div>
    </div>
@endsection
