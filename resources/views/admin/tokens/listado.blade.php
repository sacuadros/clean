
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Listado de registros creados</h3>
        <div class="box-tools pull-right">
          <!-- Buttons, labels, and many other things can be placed here! -->
          <!-- Here is a label for example -->
         
        </div>
    <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    
        <div class="box-body"> 
         		
		<passport-clients></passport-clients>
		<passport-authorized-clients></passport-authorized-clients>
		<passport-personal-access-tokens></passport-personal-access-tokens>	                    
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- box-footer -->    
</div>
<!-- /.box -->