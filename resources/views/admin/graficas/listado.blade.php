<?php $nombremes = array('','ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE'); ?>
<div  class="row" >
<br/>
	<div class="box box-primary">
		<div class="box-header">
			<div class="col-md-4">                 
	                  {{Form::label('variable','Variable',['class'=>'label label-default'])}}
	                  {!! Form::select('variable',[' '=>' '],intval($mes),['class'=>'form-control','onchange'=>'cambiar_fecha_grafica();','id'=> 'variable']) !!}                  

			</div>
			<div class="col-md-4">
	                  {{Form::label('opcion', 'Opcion',['class'=>'label label-default'])}}
	                  {!! Form::select('opcion',['anio'=>'Ano','mes'=>'Mes','dia'=>'Dia'],$anio,['class'=>'form-control','placeholder'=>'Seleccione un tipo de grafica','onchange'=>'cambiar_fecha_grafica();','id'=> 'opcion']) !!} 
  			</div>
			<div class="col-md-4">
				<div class="col-md-6">
			        {{Form::label('anio', 'Año',['class'=>'label label-default'])}}
		          	{!! Form::select('anio_sel',['2015'=>'2015','2016'=>'2016','2017'=>'2017','2018'=>'2018','2019'=>'2019'],$anio,['class'=>'form-control','placeholder'=>'Seleccione un año','onchange'=>'cambiar_fecha_grafica();','id'=> 'anio_sel']) !!}
		        </div>
		        <div class="col-md-6">          
		          	{{Form::label('mes','Mes',['class'=>'label label-default'])}}
		          	{!! Form::select('mes_sel',['1'=>'ENERO','2'=>'FEBRERO','3'=>'MARZO','4'=>'ABRIL','5'=>'MAYO','6'=>'JUNIO','7'=>'JULIO','8'=>'AGOSTO','9'=>'SEPTIEMBRE','10'=>'OCTUBRE','11'=>'NOVIEMBRE','12'=>'DICIEMBRE'],intval($mes),['class'=>'form-control','onchange'=>'cambiar_fecha_grafica();','id'=> 'mes_sel']) !!}
	          	</div>                  
			</div>
		</div>

		<div class="box-body" id="div_grafica_barras">
		</div>

	    <div class="box-footer">
		</div>
	</div>
	<div class="box box-primary">
		<div class="box-header">
			<div class="col-md-4">                 
	                  {{Form::label('variable','Variable',['class'=>'label label-default'])}}
	                  {!! Form::select('variable',[' '=>' '],intval($mes),['class'=>'form-control','onchange'=>'cambiar_fecha_grafica();','id'=> 'variable']) !!}                  

			</div>
			<div class="col-md-4">
	                  {{Form::label('opcion', 'Opcion',['class'=>'label label-default'])}}
	                  {!! Form::select('opcion',['anio'=>'Ano','mes'=>'Mes','dia'=>'Dia'],$anio,['class'=>'form-control','placeholder'=>'Seleccione un tipo de grafica','onchange'=>'cambiar_fecha_grafica();','id'=> 'opcion']) !!} 
  			</div>
			<div class="col-md-4">
				 {{Form::label('dia', 'Dia',['class'=>'label label-default'])}}				
				 {{ Form::date('dia', new \DateTime(), ['class' => 'form-control']) }}
				 {!! 'datos por hora' !!}              
			</div>
		</div>

		<div class="box-body">
		</div>

	    <div class="box-footer">
		</div>
	</div>



		<br/>
	<div class="box box-primary">
		<div class="box-header">
		</div>

		<div class="box-body" id="div_grafica_lineas">
		</div>

	    <div class="box-footer">
		</div>
	</div>


	<br/>
	<div class="box box-primary">
		<div class="box-header">
		</div>

		<div class="box-body" id="div_grafica_pie">
		</div>

	    <div class="box-footer">
		</div>
	</div>


</div>
