@extends('adminlte::page')

@section('htmlheader_title')
Graficas de mediciones
@endsection
@section('contentheader_title')
Graficas de mediciones
@endsection
@section('contentheader_description')
    
@endsection
@section('main-content')
    <div class="container-fluid spark-screen">
        @include('admin.graficas.listado')               
        @include('admin.graficas.scripts')    
    </div>

@endsection

