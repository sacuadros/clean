<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function () {
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
    Route::resource('tipounidadesmedida', 'TipoUnidadesMedidaController');
    Route::get('tipounidadesmedida/{id}/destroy',[
    	'uses' => 'TipoUnidadesMedidaController@destroy',
    	'as' => 'tipounidadesmedida.destroy'
    ]);
    Route::resource('unidadesmedida', 'UnidadesMedidaController');
    Route::get('unidadesmedida/{id}/destroy',[
    	'uses' => 'UnidadesMedidaController@destroy',
    	'as' => 'unidadesmedida.destroy'
    ]);
    Route::resource('variables', 'VariablesController');
    Route::get('variables/{id}/destroy',[
    	'uses' => 'VariablesController@destroy',
    	'as' => 'variables.destroy'
    ]);

    Route::get('dropdown', function(Request $request){
   
    $id = $request->input('option');  
    $unidades = DB::table('unidades_medida')->where('tipo_unidad_medida_fk',$id )->get();
    if($unidades->all()){
         $retorno=$unidades->pluck('nombre', 'id');
    } 
    else{
         $retorno=[''=>'No tiene unidades agregadas'];       
   }  
    return $retorno;
    });

    Route::post('mediciones',[
        'uses' => 'MedicionesController@store',
        'as' => 'mediciones.store'
    ]);
    Route::get('listado_graficas',[
    	'uses' => 'GraficasController@index',
    	'as' => 'listado_graficas.index'
    ]);   
    Route::get('grafica_registros/{anio}/{mes}', 'GraficasController@registros_mes');
    Route::get('grafica_publicaciones', 'GraficasController@total_publicaciones');
    Route::get('tokens', function () {
    return view('admin.tokens.tokens');
    });
     Route::get('live_grafica/{id}',[
        'uses' => 'HomeController@livegrafica',
        'as' => 'live_grafica.livegrafica'
    ]);  
});
