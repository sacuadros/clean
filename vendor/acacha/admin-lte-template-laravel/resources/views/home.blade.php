@extends('adminlte::layouts.app')
@section('htmlheader_title')
Inicio
@endsection
@section('contentheader_title')
Dashboard
@endsection
@section('contentheader_description')

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">			
			@include('dashboard.listado')
			@include('dashboard.scripts') 			
		</div>
	</div>
@endsection
