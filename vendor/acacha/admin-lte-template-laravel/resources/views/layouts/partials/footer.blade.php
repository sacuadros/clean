<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
       
    </div>
    <!-- Default to the left -->
    <strong> {{config('app.name')}} Copyright &copy; {{date("Y")}} </strong> 
</footer>
